#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPushButton>
#include <QLineEdit>
#include <QDebug>
#include <QRandomGenerator>
#include <QDir>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(handleGo()));
    m_Process = new QProcess(this);
    m_Process->setProgram("msrx");
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::handleGo()
{
    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    QString newPassword ="";
    for(int i=0;i<13;i++) {
        int getIndex = QRandomGenerator::securelySeeded().bounded(possibleCharacters.length());
        newPassword.append(possibleCharacters.at(getIndex));
    }
    ui->input->setText(newPassword);

    QString input = "%" + ui->input->text() + "?";
    uint8_t result=0;
    QString addMe = "";

    for(int i=0;i<input.length();i++) {
        addMe += input[i].toUpper();
        uint8_t sixVal = (((uint8_t)input[i].toUpper().toLatin1()) - 32);
        result = result ^ sixVal;
        result = result & 0x3F;
    }
    ui->output->setText(addMe + QChar(result+32));

    m_Process->start();
    m_Process->waitForFinished();
    qDebug()<<m_Process->readAllStandardError();}
